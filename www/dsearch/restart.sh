#!/bin/bash
#
# restart lighttpd

sudo cp lighttpd.conf /etc/lighttpd/
sudo pkill -KILL -u www-data lighttpd
sudo service lighttpd start
