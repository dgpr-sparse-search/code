#!/bin/bash

WWWUSER="www-data"
PIDFILE="/tmp/dsearch.pid"
SOCKET="/tmp/dsearch.sock"

if [ -f $PIDFILE ]; then
    sudo -u $WWWUSER kill `cat -- $PIDFILE`
    sudo -u $WWWUSER rm -f -- $PIDFILE
fi

exec /usr/bin/env - \
    PYTHONPATH="/usr/lib/python2.7/dist-packages/:.." \
    sudo -u $WWWUSER ./manage.py runfcgi method=threaded socket=$SOCKET pidfile=$PIDFILE umask=002
