from django.conf.urls import patterns, include, url

from django.conf.urls.static import static

from django.contrib import admin

from dsearch import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('search.urls', namespace='search')),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
