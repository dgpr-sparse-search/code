from django.conf.urls import patterns, url

from search import views

urlpatterns = patterns('',
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^browse/', views.browse, name='browse'),
    url(r'^search/', views.whoosh_search, name='search'),
)
