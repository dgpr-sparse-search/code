from django.db import models


class Image(models.Model):
    ''' the original test images
    '''
    imageid = models.IntegerField(unique=True)
    filename = models.ImageField()
    def __unicode__(self):
        return self.filename.name

class ImageHist(models.Model):
    ''' the graphical representations of the test images
    '''
    histid = models.IntegerField(unique=False)
    filename = models.ImageField()
    bins = models.IntegerField(unique=False)
    def __unicode__(self):
        return self.filename.name

class Result(models.Model):
    ''' result of a browse or search action
    '''
    image = models.ForeignKey(Image)
    score = models.FloatField()
    def __unicode__(self):
        return self.result.filename.name + u'[%.3f]'%self.score
