not empty


<ul class="list-inline">
 <li>
  <h5>Bin Size</h5>
  <form class="form">
  <div class="btn-group btn-toggle" data-toggle="bins">
    <label class="btn btn-default">
      <input type="radio" name="bins" value="32"> 32
    </label>
    <label class="btn btn-default">
      <input type="radio" name="bins" value="64" checked=""> 64
    </label>
    <label class="btn btn-default">
      <input type="radio" name="bins" value="128" checked=""> 128
    </label>
  </div>                                                          
  </form>
 </li>
 <li>
  <h5>Transformer</h5>
  <form class="form">
  <div class="btn-grou  btn-toggle" data-toggle="buttons">
    <label class ="btn btn-default">
      <input type ="radio" name = "transformer" value="gaussian"> Gaussian
    </label>
    <label class ="btn btn-default">
      <input type="radio" name="transformer" value="fourier"> Fourier
    </label>
  </form>                                                                       
 </li>
</ul>                                                                            


 {% for radio in GetImageForm.bins %}
 <div class="myradio">
         {{ radio }}
 </div>
 {% endfor %}
                                             
                                             
 {% for radio in GetImageForm.transformer %}
 <div class="myradio">
         {{ radio }}
 </div>
 {% endfor %}                               







{% extends 'search/base.html' %}

{% block title %}
Image Search
{% endblock %}

{% block content %}
<form role="form" method="post" action=".">

       <div class="caption">
         Search Time: {{ search_time }} seconds     Transform Time: {{ transform_time }} seconds
       </div>
     </li>
   </ul>
     <div class="row">
        {% for image in images %}
        <div class="col-md-2">
            <div class="thumbail">
                <img src="{{ MEDIA_URL }}{{ image.filename }}" class="img-responsive"
                    id="{{ image.imageid }}">
            </div>
            <div class="caption">
                [{{ image.imageid }}] {{ image.filename }}
            </div>
        </div>
        {% if forloop.counter|divisibleby:6 %}
    </div>
    <div class="row">
        {% endif %}
        {% endfor %}
    </div>

  {% for hist in hists %}
  <div class ="col-md-2">
      <div class = "thumbail">
          <img src="{{ MEDIA_URL }}{{ hist.filename }}" class="img-responsive"
              id ="{{ hist.histid }}">
      </div>
  </div>                                                                      
  {% endfor %}

</div>
{% endif %}
{% endblock %}

{% block js %}
<script>
$(document).ready(function(){
    $('#results').on('click', function(event) {
        window.location.href = '/search/?imageid='+event.target.id
    });
});
</script>
{% endblock %}                                                                                          
