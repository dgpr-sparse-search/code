from django import forms

class GetImageRangesForm(forms.Form):
    imageids = forms.CharField(required=False, label='imageids',\
            widget=forms.Textarea(attrs={'class' : 'form-control', 'rows' : 3,\
                                         'placeholder' : 'image ranges e.g. 1-3,4,5,9-12'}))

BIN_CHOICES = (('32', '32',),('64','64',),('128','128',))

TRANSFORMER_CHOICES = (('gaussian', 'gaussian',),('fourier','fourier',))

class GetImageForm(forms.Form):
    imageids= forms.IntegerField(required=False, label='imageids',
            widget=forms.TextInput(attrs={'class' : 'form-control',
            'placeholder' : 'Sorry, the search feature doesn\'t work well yet, but we\'re hard at work :)'}))
    
    bins= forms.ChoiceField(choices=BIN_CHOICES)

    transformer= forms.ChoiceField(choices=TRANSFORMER_CHOICES)
