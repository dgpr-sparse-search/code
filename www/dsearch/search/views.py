from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist

from django.views.generic import TemplateView

from search.forms import GetImageForm, GetImageRangesForm
from search.models import Image, ImageHist, Result

from imageclef.simplesearch import similarto

#whoosh stuff
from whoosh.index import create_in, open_dir
from whoosh.fields import ID, KEYWORD, Schema
from whoosh.qparser import QueryParser, OrGroup
import indexer.histograms as ih
import os
import numpy as np
import time
from scipy.fftpack import dct

class HomeView(TemplateView):
    template_name = "search/home.html"


def browse(request):

    def _process(imageidranges):
        _images = None
        if not imageidranges:
            return images

        ranges = (x.split("-") for x in imageidranges.split(","))
        imageids = [i for r in ranges for i in range(int(r[0]), int(r[-1]) + 1)]

        _images = []
        for imageid in imageids:
            try:
                nextimage = Image.objects.get(imageid=imageid)
            except ObjectDoesNotExist:
                default_image = Image.objects.get(imageid=0)
                nextimage = Image(imageid=imageid, filename=default_image.filename)
            _images.append(nextimage)
        return _images                                                               

    images = None
    if request.method == 'GET':
        form = GetImageRangesForm(request.GET)
        if form.is_valid():
            imageidranges = form.cleaned_data['imageids']
            images = _process(imageidranges)
    else:
        form = GetImageRangesForm() # An unbound form
    return render(request, 'search/browse.html', {'form': form, 'images' : images })


def whoosh_search(request):
    
    def whoosh(imageid__):

        def indices_as_keywords(n):
            return u' '.join(map(unicode, list(n)))

        class GaussianTransformer(object):
            def __init__(self, d, m, h, random_seed):
                np.random.seed(random_seed)
                self.A = np.random.randn(m, d)
                self.h = h
        
            def transform(self, x):
                return indices_as_keywords(np.nonzero(np.dot(self.A, x)>self.h)[0])
        
        class SparseFFTTransformer(object):
            def __init__(self, d, m, h, random_seed):
                np.random.seed(random_seed)
                self.choices = np.random.choice(m, size=d, replace=False)
                self.signs = 1.0-2.0*np.random.randint(2, size=d)
                self.h = h*np.sqrt(2.)  # equivalent to scaling dct by 1/sqrt(2)
                self.y = np.zeros(m)
        
            def transform(self, x):
                self.y[self.choices] = self.signs*x
                return indices_as_keywords(np.nonzero(dct(self.y)>self.h)[0])
        
        histogram_file = '/var/data/imageclef/histograms/hsv{:d}/1/histo_numbers.json'.format(int(bin_toggle))
        from ujson import load
        with open(histogram_file, 'r') as histogram_json:
            histograms = load(histogram_json)
    
        def normalize_list(l):
            x = np.array(l, dtype=float)
            return x/np.linalg.norm(x)

        normalized_histograms = {k:normalize_list(h) for k, h in histograms.iteritems()}

        params = {
            'd': 128,
            'm': 50000,
            'h': 2.0
        }
        base_data_dir = '/home/treimer/var/data/whoosh_index'

        gaussian_transformer = GaussianTransformer(int(bin_toggle), params['m'], 2.8, random_seed=0)
        sparsefft_transformer = SparseFFTTransformer(int(bin_toggle), params['m'], 2.5, random_seed=0)
        transformer = gaussian_transformer
        #transformer = sparsefft_transformer

        if transformer == gaussian_transformer:
               
            gaussian_index_dir = os.path.join(base_data_dir, 'gaussian/{:d}'.format(int(bin_toggle)))
            gaussian_ix = open_dir(gaussian_index_dir)
            ix = gaussian_ix

        if transformer == sparsefft_transformer:
    
            sparsefft_index_dir = os.path.join(base_data_dir, 'sparsefft/{:d}'.format(int(bin_toggle)))
            sparsefft_ix = open_dir(sparsefft_index_dir)
            ix = sparsefft_ix

        schema = Schema(path=ID(stored=True), content=KEYWORD)

        seed_hist = normalized_histograms[''+`imageid__`+'']

        transform_time = 0
        search_time = 0

        with ix.searcher() as searcher:
            _t = time.time()
            seed = transformer.transform(seed_hist)
            transform_time += (time.time() - _t)
            query = QueryParser("content", ix.schema, group=OrGroup).parse(seed)
            _t = time.time()
            results = searcher.search(query, limit=20)
            search_time += (time.time() - _t)
            results_ = []
            for k, r in enumerate(results):
                result_id = results[k]['path']
                results_.append(int(result_id))

        search_time = ("%.2f" % search_time) 
        return results_, search_time, transform_time

                                                                          
    def _process(results_):

        _images = []
        for imageid in results_:
            try:
                nextimage = Image.objects.get(imageid=imageid)
                nexthist = ImageHist.objects.get(histid = imageid, bins = bin_toggle)
            except ObjectDoesNotExist:
                default_image = Image.objects.get(imageid=0)
                nextimage = Image(imageid=imageid, filename=default_image.filename)
            _images.append(nextimage)
            _images.append(nexthist)
        return _images                                                               
    

    def _process_hists (results_):
                                                                                      
        _hists = []
        for histid in results_:
            try:
                nextimage = ImageHist.objects.get(histid = histid, bins = bin_toggle)
            except ObjectDoesNotExist:
                default_hist = ImageHist.objects.get(histid=0)
                nextimage = ImageHist(histid=histid, filename=default_hist.filename)
            _hists.append(nextimage)
        return _hists                                                               

    images = None
    results_ = None
    hists = None
    search_time = 0
    transform_time = 0
    bin_toggle = None
    transformer_toggle = None

    if request.method == 'POST':
        form = GetImageForm(request.POST)
        if form.is_valid():
            imageid__ = form.cleaned_data['imageids']
            bin_toggle = form.cleaned_data['bins']
            transformer_toggle = form.cleaned_data['transformer']
            
            #bin_toggle = 128
            #transformer_toggle='gaussian'

            results_, search_time, transform_time  = whoosh(int(imageid__))
            images = _process(results_)
            hists = _process_hists(results_)
    else:
        form = GetImageForm() # An unbound form
    return render(request, 'search/whoosh_search.html', {'form': form, 'images' : images, 'hists' : hists, 'search_time' : search_time, 'tranform_time' : transform_time})










            #print result_id, results.score(k), np.dot(seed_hist, normalized_histograms[result_id])
        
        #x = np.array([results.score(k) for k, _ in enumerate(results)])
        #y = np.array([np.dot(seed_hist, normalized_histograms[results[k]['path']]) for k, _ in enumerate(results)])
    
        # print ','.join([results[k]['path'] for k, _ in enumerate(results)])
        #print("search_time: {:.1f} ms\ntransform_time: {:.1f} ms".format(1000.*search_time, 1000.*transform_time))




