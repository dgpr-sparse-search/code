import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


import sys, os

from django.core.management.base import BaseCommand, CommandError

from search.models import Image
from dsearch.settings import MEDIA_ROOT, DEFAULT_IMAGE, BASE_DIR

from imageclef.config import imageclef_root


class Command(BaseCommand):
    args = 'imagefile.jpg'
    help = 'add default image at imageid=0'

    def handle(self, *args, **options):
        ''' add images to the database and point them to /media/
        '''
        if len(args)>1 and args[0]=='-h':
            logger.info("usage: manage.py setdefaultimage -h")
            return

        img, created = Image.objects.update_or_create(imageid=0, 
                defaults={'filename': DEFAULT_IMAGE})
        abs_path = os.path.join(BASE_DIR, 'media', DEFAULT_IMAGE)
        www_path = os.path.join(MEDIA_ROOT, DEFAULT_IMAGE)

        sys_cmd = 'sudo cp -f %s %s && sudo chown www-data:www-data %s'%(abs_path, www_path, www_path)
        logger.info("running command: %s"%sys_cmd)
        os.system(sys_cmd)
