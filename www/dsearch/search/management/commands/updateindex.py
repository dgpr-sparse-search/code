import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


import sys, os

from django.core.management.base import BaseCommand, CommandError
from search.models import Image

from imageclef import ImageData
from imageclef.config import imageclef_root


class Command(BaseCommand):
    args = 'index count'
    help = 'index the next count images'

    def handle(self, *args, **options):
        ''' index images for particular index
        '''
        if len(args)<2:
            logger.error("usage: manage.py updateindex -h")
            return
        indexname = args[0]
        count = int(args[1])

