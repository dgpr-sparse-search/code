import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


import sys, os

from django.core.management.base import BaseCommand, CommandError
from search.models import Image
from dsearch.settings import MEDIA_ROOT

from imageclef import ImageData
from imageclef.config import IMAGECLEF_ROOT


class Command(BaseCommand):
    args = 'startpos endpos'
    help = 'add images to the database'

    def handle(self, *args, **options):
        ''' add images to the database and point them to /media/
        '''
        if len(args)<2:
            logger.error("usage: manage.py registerimages -h")
            return
        startpos = int(args[0])
        endpos = int(args[1])

        logger.info("loading images at posn: [%u:%u]"%(startpos, endpos))
        for xml_root in ImageData().itermetadata(startpos, endpos):
            imageid = int(xml_root.get('id'))
            imagefile = xml_root.get('file')

            img, created = Image.objects.update_or_create(imageid=imageid, 
                    defaults={'filename': imagefile})
            abs_path = os.path.join(IMAGECLEF_ROOT, imagefile)
            www_path = os.path.join(MEDIA_ROOT, imagefile)

            sys_cmd = 'sudo -u www-data ln -fs %s %s'%(abs_path, www_path)
            logger.info("running command: %s"%sys_cmd)
            os.system(sys_cmd)
