import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


import sys, os, ast

from django.core.management.base import BaseCommand, CommandError
from search.models import Image

from imageclef import ImageData
from imageclef.config import imageclef_root


class Command(BaseCommand):
    args = 'index parameters'
    help = 'initialize or reset index; parameters is a dictionary'

    def handle(self, *args, **options):
        ''' index images for particular index

            parameters = {
                'cutoff' : '(hard|soft)',
                'threshold' : float,
                'dimension' : int,
                'mapping' : '(gaussian|ternary|binary)',
                'mapping-parameters' : dict,
                }
        '''
        if len(args)<3:
            logger.error("usage: manage.py initindex -h")
            return
        indexname = args[0]
        param_str = args[1]
        parameters = ast.literal_eval(args[1])

        index,created = Index.objects.get_or_create(name=indexname)
        if not created and not raw_input('Overwrite index %s? [Y/n]'%indexname)==r'Y'
            logger.info('aborting')
            return
        index.parameters = param_str
