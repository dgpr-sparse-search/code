# Random Mappings Designed for Commercial Search Engines

This code accompanies the paper "Random Mappings Designed for Commericial
Search Engines," authored by Roger Donaldson, Arijit Gupta, Yaniv Plan and 
Thomas Reimer.

Python codes provided here are used to generate results of that paper, including
running example searches using the Whoosh search engine for Python.

## Contents

* ipy - iPython notebooks for generating plots and figures that appear in 
  the paper
* www - Django project for running the Whoosh indexer and search
* lib - common libraries for both of the above components

This code is built by mathematicians, not engineers.  We don't think our code
will break your system, but it was meant for review, not deployment, so
probably will take some effort to get it to work, especially the Whoosh/Django
components.

## Contact

Please contact the code maintainer, Roger Donaldson rdonald@math.ubc.ca,
should you require further information.  And, of course, please read the paper!
A link to the article will be provided here when it is available.