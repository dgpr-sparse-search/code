
import numpy as np
from scipy.fftpack import dct

class RandomTransformer(object):
    def __init__(self, d, m, r, random_state):
        self.random_state = random_state
        self.h = np.sqrt(2.*r*np.log(m))
        self.d = d
        self.m = m
        
    def transform(self, x):
        raise NotImplemented
    
    def threshold(self, x):
        return self.transform(x)>self.h
    
    def sparse(self, x):
        return set(np.nonzero(self.threshold(x))[0])
    
    def dense(self, x, dtype=int):
        return np.array(self.threshold(x), dtype=dtype)
    
    def score(self, x, y):
        return np.sum(np.logical_and(self.dense(x, dtype=bool), self.dense(y, dtype=bool)))/float(self.m)
    
    def indices_as_keywords(self, x):
        return u' '.join(map(unicode, list(self.sparse(x))))

class GaussianTransformer(RandomTransformer):
    name = "Gaussian"
    
    def __init__(self, d, m, r, random_state):
        super(GaussianTransformer, self).__init__(d, m, r, random_state)
        self.A = self.random_state.normal(size=(m, d))
        
    def transform(self, x):
        return np.dot(self.A, x)
    
class StructuredTransformer(RandomTransformer):
    name = "Biased Structured"
    
    def __init__(self, d, m, r, random_state):
        super(StructuredTransformer, self).__init__(d, m, r, random_state)
        self.choices = self.random_state.choice(m, size=d, replace=False)
        self.signs = 1.0-2.0*self.random_state.randint(2, size=d)
        self.y = np.zeros(m)
        
    def transform(self, x):
        self.y[self.choices] = x*self.signs
        return dct(self.y, type=2, norm='ortho')*np.sqrt(self.m)
    
class BlockStructuredTransformer(RandomTransformer):
    name = "Structured"
    
    def __init__(self, d, m, r, random_state):
        assert not m%d, "BlockStructuredTransformer: m must be a multiple of d"
        super(BlockStructuredTransformer, self).__init__(d, m, r, random_state)
        
        # random sign changes: each row will apply a random set of sign changes to input x
        self.signs = 1.0-2.0*self.random_state.randint(2, size=(m/d, d))
        
    def transform(self, x):
        # use np.multiply's broadcasting to apply each row of sign changes to input x
        return dct(np.multiply(self.signs, x).flatten(), type=2, norm='ortho')*np.sqrt(self.d)


def index_json(json_file, params, transformer_, out_directory):
    
    #from whoosh.index import create_in, open_dir
    import whoosh.index as wi
    #import whoosh.fields as wf
    from whoosh.fields import ID, KEYWORD, Schema
    import numpy as np
    import time
    from ujson import load
    
    with open(json_file, 'r') as histogram_json:
        histograms = load(histogram_json)

    def normalize_list(l):
        x = np.array(l, dtype=float)
        return x/np.linalg.norm(x)

    normalized_histograms = {k:normalize_list(h) for k, h in histograms.iteritems()}
    
    schema = Schema(path=ID(stored=True), content=KEYWORD)

    if transformer_ == 'gaussian':
        gaussian_transformer = GaussianTransformer(params['d'], params['m'], params['r'], np.random.RandomState(0))
        transformer = gaussian_transformer
        gaussian_index_dir = out_directory
        ix = wi.create_in(gaussian_index_dir, schema)
        gaussian_ix = wi.open_dir(gaussian_index_dir)
        print transformer.name

    if transformer_ == 'structured':
        transformer = BlockStructuredTransformer(params['d'], params['m'], params['r'], np.random.RandomState(0))
        sparsefft_index_dir = out_directory
        ix = wi.create_in(sparsefft_index_dir, schema)
        sparsefft_ix = wi.open_dir(sparsefft_index_dir)
        print transformer.name

    transformer_time = 0
    writer_time = 0
    commit_time = 0

    writer = None
    nb_documents = len(normalized_histograms)
    for k, (img_id, doc) in enumerate(normalized_histograms.iteritems()):

        if not writer:
            writer = ix.writer()

        _t = time.time()
        content = transformer.indices_as_keywords(doc)
        transformer_time += (time.time() - _t)

        _t = time.time()
        writer.add_document(path=u'{}'.format(img_id), content=content)
        writer_time += (time.time() - _t)

        #if not (k+1)%100:
            #print("indexed {}/{} documents".format(k+1, nb_documents))

        if not (k+1)%1000 or (k+1)==nb_documents:
            print("indexed {}/{} documents".format(k+1, nb_documents))
            #print("committing index")
            _t = time.time()
            writer.commit()
            commit_time += (time.time() - _t)
            writer = None

    print("transformer time: {:.3f} s\nwriter_time: {:.3f} s\ncommit_time: {:.3f} s".format(transformer_time, writer_time, commit_time))


def search(seed, json_file, params, transformer_, in_directory, nb_results):

    #from whoosh.index import create_in, open_dir
    import whoosh.index as wi
    from whoosh.fields import ID, KEYWORD, Schema
    from whoosh.qparser import QueryParser, OrGroup
    import numpy as np
    import time
    from ujson import load
    
    with open(json_file, 'r') as histogram_json:
        histograms = load(histogram_json)

    def normalize_list(l):
        x = np.array(l, dtype=float)
        return x/np.linalg.norm(x)

    normalized_histograms = {k:normalize_list(h) for k, h in histograms.iteritems()}
    
    schema = Schema(path=ID(stored=True), content=KEYWORD)
    
    if transformer_ == 'gaussian':
        transformer = GaussianTransformer(params['d'], params['m'], params['r2'], np.random.RandomState(0))
        gaussian_index_dir = in_directory
        gaussian_ix = wi.open_dir(gaussian_index_dir)
        ix = gaussian_ix
        
    if transformer_ == 'structured':
        transformer = BlockStructuredTransformer(params['d'], params['m'], params['r2'], np.random.RandomState(0))
        sparsefft_index_dir = in_directory
        sparsefft_ix = wi.open_dir(sparsefft_index_dir)
        ix = sparsefft_ix

    
    seed_hist = normalized_histograms[seed]

    transform_time = 0
    search_time = 0

    with ix.searcher() as searcher:
        _t = time.time()
        seed = transformer.indices_as_keywords(seed_hist)
        transform_time += (time.time() - _t)
        query = QueryParser("content", ix.schema, group=OrGroup).parse(seed)
        _t = time.time()
        results = searcher.search(query, limit=nb_results)
        search_time += (time.time() - _t)
        results_ = []
        for k, r in enumerate(results):
            result_id = results[k]['path']
            #print result_id, results.score(k), np.dot(seed_hist, normalized_histograms[result_id])
            results_.append(result_id)
        x = np.array([results.score(k) for k, _ in enumerate(results)])
        y = np.array([np.dot(seed_hist, normalized_histograms[results[k]['path']]) for k, _ in enumerate(results)])

        #print ','.join([results[k]['path'] for k, _ in enumerate(results)])

    #print("search_time: {:.1f} ms\ntransform_time: {:.1f} ms".format(1000.*search_time, 1000.*transform_time))
    return results_, search_time
